<?php

namespace Ocw\ApiSession;

use Illuminate\Support\ServiceProvider;
use Ocw\ApiSession\ApiSession;

class ApiSessionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('api-session', function() {
            return new ApiSession();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
