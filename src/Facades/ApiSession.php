<?php

namespace Ocw\ApiSession\Facades;

use Illuminate\Support\Facades\Facade;

class ApiSession extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'api-session';
    }
}
