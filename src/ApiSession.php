<?php

namespace Ocw\ApiSession;

class ApiSession
{
    public $api_session_key_prefix;
    public $skip = [];
    public $ttl = 3600;

    public function __construct()
    {
        $token = request()->header('authorization')."-".request()->header('CompanyID'); //for logged in user
        $ss = request()->header('ss'); //for outside links
        $prefix_value = $token ?? $ss ?? request()->ip(); //defults to user's ip
        $this->api_session_key_prefix = hash('SHA256',$prefix_value);
    }

    public function put($key, $value)
    {
        $cache_key = $this->api_session_key_prefix . ":" . $key;
        \Cache::put($cache_key, $value, $this->ttl);
    }

    public function get($key, $value = null)
    {
        $cache_key = $this->api_session_key_prefix . ":" . $key;
        return \Cache::get($cache_key, $value);
    }

    public function forget($key)
    {
        $cache_key = $this->api_session_key_prefix . ":" . $key;
        \Cache::forget($cache_key);
    }

    public function has($key)
    {
        $cache_key = $this->api_session_key_prefix . ":" . $key;
        \Cache::has($cache_key);
    }



    public function flush()
    {
        //to do if required
        // $cache_key = $this->api_session_key_prefix;
        // \Cache::forget($cache_key);
    }
}
