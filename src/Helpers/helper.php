<?php

if (! function_exists('apiSession')) {
    /**
     * Get / set the specified session value.
     *
     *
     * @param  string|null  $key
     * @param  mixed  $default
     *
     */
    function apiSession($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('api-session');
        }
        return app('api-session')->get($key, $default);
    }
}

